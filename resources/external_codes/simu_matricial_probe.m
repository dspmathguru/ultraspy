%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Settings

geometry = 'matricial';
% - single-scat: One scatterer, centered
% - many-scat: One scatterer in negative lateral, + four in positive
% - many-random-scat: 20 random scatterers, with random amplitudes
medium = 'many-scat';
% - 1pw: Only one plane wave is shot
% - lat-5pw: Five steered plane waves are shot along lateral axis
% - elev-5pw: Five steered plane waves are shot along elevational axis
% - both-13pw: Thirteen steered plane waves are shot on both directions
compounding = 'both-13pw';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load a probe

probe_name = 'MUX_1024_8MHz';
[param, rect] = MUX_getparam();
param.c = 1540;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Build a medium

if strcmp(medium, 'single-scat')
    nscat = 1;
    x = [0]; y = [0]; z = [25e-3];
elseif strcmp(medium, 'many-scat')
    nscat = 8;
    x = [-5e-3,  2e-3,  4e-3,  6e-3,    0,   2e-3,  4e-3,  6e-3];
    y = [    0,     0,     0,     0,    0,   2e-3,  4e-3,  6e-3];
    z = [15e-3, 25e-3, 25e-3, 25e-3, 35e-3, 35e-3, 35e-3, 35e-3];
elseif strcmp(medium, 'many-random-scat')
    nscat = 20;
    x = rand(1, nscat) * 10e-3 - 5e-3;
    y = rand(1, nscat) * 10e-3 - 5e-3;
    z = rand(1, nscat) * 20e-3 + 15e-3;
end

[Y_scat, X_scat, Z_scat] = meshgrid(y, x, z);
%scatterers.pos = [X_scat(:), Y_scat(:), Z_scat(:)];
scatterers.pos = [x(:), y(:), z(:)];
scatterers.A = ones(numel(x), 1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Plane waves

if strcmp(compounding, '1pw')
    angles = [[0, 0];];
elseif strcmp(compounding, 'lat-5pw')
    angles = [[-10, 0]; [-5, 0]; [0, 0]; [5, 0]; [10, 0]];
elseif strcmp(compounding, 'elev-5pw')
    angles = [[0, -10]; [0, -5]; [0, 0]; [0, 5]; [0, 10]];
elseif strcmp(compounding, 'both-13pw')
    angles = [[-10, 0]; [-5, 0]; [0, 0]; [5, 0]; [10, 0];
              [0, -10]; [0, -5]; [0, 5]; [0, 10];
              [-5, -5]; [-5, 5]; [5, -5]; [5, 5]];
end
nb_a = size(angles, 1);
param.TX_angles = angles;
param.TXsteer = deg2rad(param.TX_angles);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Transmitted pulse

t = (0:1/param.fs:4/param.f0);
pulse_init = sin(2*pi*param.f0*t).*(hanning(length(t))');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Field simulation

field_init(0);
set_field('fs', param.fs);
set_field('c', param.c);

% Containers
t0s = [];
Nts = [];
RFs = cell(nb_a, 1);
delays = zeros(nb_a, param.Nelements);

% Actual simulation
for angle = 1:nb_a
    TX = xdc_rectangles(rect, param.elements, angles2focus(param.TXsteer(angle, :)));
    xdc_impulse(TX, 1); xdc_excitation(TX, pulse_init);

    TX_focus = xdc_get(TX, 'focus');
    delays(angle, :) = TX_focus(2:end);

    % Receive probe
    RX = xdc_rectangles(rect, param.elements, angles2focus([0, 0]));
    xdc_impulse(RX, 1);

    [rf, t0] = calc_scat_multi(TX, RX, scatterers.pos, scatterers.A);

    pulse_total = conv(conv(pulse_init, 1), 1);
    demi_long = round(length(pulse_total)/2);

    t0s = [t0s, t0 - demi_long/param.fs];
    Nts = [Nts, size(rf, 1)];
    RFs{angle} = rf;

    xdc_free(TX); xdc_free(RX);
end

param.t0 = min(t0s);
ts = 1/param.fs;

% Add 0 samples before signals
for angle = 1:size(param.TX_angles, 1)
    [nnt, nne] = size(RFs{angle});
    diff = round((t0s(angle) - param.t0) / ts);
    RFs{angle} = cat(1, zeros(diff, nne), RFs{angle});
    Nts(angle) = size(RFs{angle}, 1);
end

% Add 0 samples after signals
param.Nt = max(Nts);
for angle = 1:size(param.TX_angles, 1)
    [nnt, nne] = size(RFs{angle});
    RFs{angle} = cat(1, RFs{angle}, zeros(param.Nt - nnt, nne));
end

field_end;

RF = cat(3, RFs{:});


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Save

param.probe_name = probe_name;
param.delays = delays;
save(strcat('simu_', geometry, '_', medium, '_', compounding, '.mat'), 'RF', 'param');






% %% Beamforming
% x = linspace(-2, 2, 81);
% y = [0];
% z = linspace(-1, 1, 41) + z_scat;
%
% [Y, X, Z] = meshgrid(y*1e-3, x*1e-3, z*1e-3);
%
% iq = rf2iq(rf, param.fs, param.f0, param.bandwidth);
%
% save(join(['../data/Raw signals/' simulation_name '.mat'], ''), 'x', 'y', 'z', 'X', 'Y', 'Z', 'param', 'rf', 'iq', 'scatterers')
