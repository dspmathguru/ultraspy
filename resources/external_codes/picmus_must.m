format long


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load file

file = 'resolution_distorsion_simu_dataset_rf.hdf5';

angles = h5read(file, '/US/US_DATASET0000/angles');
initial_time = h5read(file, '/US/US_DATASET0000/initial_time');
probe_geometry = h5read(file, '/US/US_DATASET0000/probe_geometry');
sampling_frequency = h5read(file, '/US/US_DATASET0000/sampling_frequency');
sound_speed = h5read(file, '/US/US_DATASET0000/sound_speed');
data = h5read(file, '/US/US_DATASET0000/data/real');

angles = double(angles);
initial_time = double(initial_time);
probe_geometry = double(probe_geometry);
sampling_frequency = double(sampling_frequency);
sound_speed = double(sound_speed);
data = double(data);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Build a param struct for MUST

param = struct();
nb_e = size(probe_geometry, 1);
central_frequency = sampling_frequency / 4;
param.fs = sampling_frequency;
param.fc = central_frequency;
param.kerf = 0.03e-3;
param.width = 0.27e-3;
param.pitch = 0.3e-3;
param.Nelements = nb_e;
param.bandwidth = 67;
param.t0 = initial_time;  % Change this for the t0 tests
param.fnumber = 1;        % Change this for the f_number tests
param.c = sound_speed;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Select data and convert to I/Qs

indices = [38];
%indices = [1, 38, 75];   % Uncomment for 3 plane waves
nb_a = size(indices, 2);
RF = cell(nb_a, 1);
IQ = cell(nb_a, 1);
for k = 1:nb_a
    ia = indices(k);
    RF{k} = data(:, :, ia);
    IQ{k} = rf2iq(data(:, :, ia), param);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Grid of interest

x = linspace(-20e-3, 20e-3, 500);
z = linspace(5e-3, 50e-3, 1000);
[xi, zi] = meshgrid(x, z);
beamformed = zeros(1000, 500, nb_a);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Actual beamform, either RF or I/Qs

for k = 1:nb_a
    % Delays are centered in PICMUS
    delays = txdelay(param, angles(indices(k)));
    delays = -(delays - max(delays) / 2);
    % Can change the interpolation method here to 'nearest'
    % Can use 'IQ{k}' if working on I/Qs.
    beamformed(:, :, k) = das(RF{k}, xi, zi, delays, param, 'linear');
end
beamformed = sum(beamformed, 3);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Save
save('beamformed_must.mat', 'beamformed')
