"""Recover the resources to test the code from a nextcloud, this is mandatory
to run tox.
"""
import os
import urllib.request
import py7zr
from pathlib import Path

# Set variables
base = "https://www.creatis.insa-lyon.fr/nextcloud"
nextcloud_url = f"{base}/index.php/s/HRz7FFBxSPHqeby"
file_name = "resources.7z"

# Set file paths
current_dir = Path(os.getcwd())
file_path = current_dir / file_name

# Download the file
urllib.request.urlretrieve(f"{nextcloud_url}/download/{file_name}", file_path)

# Extract the file
with py7zr.SevenZipFile(file_path, mode='r') as z:
    z.extractall()

# Remove the file
os.remove(file_path)
