Contribute section
=======================

.. _contribute:

This section describes everything you should know in order to contribute to the
`ultraspy` lib. The first section aims to make sure your system is ready and
that the code works properly. Once this is ensured, the few other sections are
here to explain how to contribute to the code with your own algorithm, and,
finally, the last section details the DevOps routine of the lib, and how to
properly send push requests.

.. toctree::
   :maxdepth: 1

   preparation
   tdd
   own_setups_options
   own_beamformer
   own_methods
   devops_routine
