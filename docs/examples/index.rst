.. _examples:

Examples of application
-----------------------
Four tutorials are proposed to help any new user to understand how the package
works and how to use it easily. It is strongly advised to follow them before
using `ultraspy`.


.. toctree::
   :maxdepth: 1

   das
   doppler
   metrics
   simu_data
   torch_compatibility
