"""Routine to perform a simple Doppler operation on RFs from a rotating disk.
"""
import numpy as np
import cupy as cp
import matplotlib.pyplot as plt

from ultraspy.io.reader import Reader
from ultraspy.scan import GridScan
from ultraspy.beamformers.das import DelayAndSum
import ultraspy as us


# Load the data
reader = Reader('/path/to/rotating_disk.mat', 'must')

# Medium to observe
x = np.linspace(-12.5, 12.5, 250) * 1e-3
z = np.linspace(10, 35, 250) * 1e-3
scan = GridScan(x, z)

# DAS Beamformer, with the I/Q option set to True, as we need to work on
# phase-shifts for Doppler
beamformer = DelayAndSum(is_iq=True)
beamformer.automatic_setup(reader.acquisition_info, reader.probe)

# Send the whole packet to GPU, as complex data so we can store I/Qs
d_data = cp.asarray(reader.data, np.complex64)

# Conversion, required some info about the acquisition
info = beamformer.setups
us.rf2iq(d_data, info['central_freq'], info['sampling_freq'], beamformer.t0)

# Beamforming
d_beamformed_packet = beamformer.beamform_packet(d_data, scan)

# We need to know the nyquist velocity for the color-map
nyquist = info['sound_speed'] * info['prf'] / (4 * info['central_freq'])

# Doppler maps
d_color_map = us.get_color_doppler_map(d_beamformed_packet, nyquist)
d_power_map = us.get_power_doppler_map(d_beamformed_packet)
color_map = d_color_map.get()
power_map = d_power_map.get()

# Get one B-Mode for display
d_last_beamformed = d_beamformed_packet[..., -1].copy()
d_envelope = beamformer.compute_envelope(d_last_beamformed, scan)
us.to_b_mode(d_envelope)
b_mode = d_envelope.get()

# Power threshold for color map
power_threshold = -20
doppler_colormap = us.get_doppler_colormap()

# Display init
extent = [x * 1e3 for x in [x[0], x[-1], z[-1], z[0]]]  # In mm
fig, axes = plt.subplots(1, 2)

# Color map, we show the B-Mode first, then the masked color-map
color_map = np.ma.masked_where(power_map < power_threshold, color_map)
axes[0].imshow(b_mode.T, extent=extent, cmap='gray', clim=[-60, 0])
im1 = axes[0].imshow(color_map.T, extent=extent, cmap=doppler_colormap,
                     clim=[-nyquist, nyquist])
fig.colorbar(im1, ax=axes[0])
axes[0].set_title('Color map (doppler velocity (m/s))')

# Power map
im2 = axes[1].imshow(power_map.T, extent=extent, cmap='hot', clim=[-30, 0])
axes[1].set_title('Power map (dB)')
fig.colorbar(im2, ax=axes[1])

plt.show()
